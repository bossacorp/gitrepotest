
public class TennisGame {
	
	//Setup
	int puntosA, puntosB = 0;
	String marcadorA = "Love";
	String marcadorB = "Love";
	
	public String getscore() {
		
		if(puntosA == 3 && puntosB == 3)
			return "Deuce";
		else
			return marcadorA+"-"+marcadorB;
	}
	
	public void puntoPara(String player){
		if(player.equals("P1"))
			puntosA++;
		else
			puntosB++;
		
		
		if(puntosA == 1) marcadorA = "Fifteen";
		if(puntosA == 2) marcadorA = "Thirty";
		if(puntosA == 3) marcadorA = "Forty";
		
		if(puntosB == 1) marcadorB = "Fifteen";
		if(puntosB == 2) marcadorB = "Thirty";
		if(puntosB == 3) marcadorB = "Forty";
	}
}
