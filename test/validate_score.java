import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;


public class validate_score {
	
	TennisGame juego;
	
	@Before
	public void setup(){
		juego = new TennisGame();
	}

	@Test
	public void validar_inicio_juego() {
		String result = "Love-Love";
		
		assertEquals(result, juego.getscore());
		
	}

	@Test
	public void validar_forty_love() {
		String result = "Forty-Love";		
		
		juego.puntoPara("P1");
		juego.puntoPara("P1");
		juego.puntoPara("P1");
		
		
		assertEquals(result, juego.getscore());
		
		
	}
	
	@Test
	public void validarDeuce(){
		String result = "Deuce";
		
		juego.puntoPara("P1");
		juego.puntoPara("P1");
		juego.puntoPara("P1");
		
		juego.puntoPara("P2");
		juego.puntoPara("P2");
		juego.puntoPara("P2");
		
		assertEquals(result, juego.getscore());
	}
	
}
